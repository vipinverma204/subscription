package com.tm.spring.questionnaire.subscription.controllers.subscription;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tm.spring.questionnaire.subscription.services.subscription.SubscriptionService;

@Controller
public class SubscriptionController {
	
	@Autowired
	SubscriptionService subscriptionService;
	
	@RequestMapping(value="/subscribe", method=RequestMethod.GET)
	public ResponseEntity<String> subscribe(HttpServletRequest request) {
		return new ResponseEntity<String>(subscriptionService.subscribe(request), HttpStatus.OK);
	}

}
