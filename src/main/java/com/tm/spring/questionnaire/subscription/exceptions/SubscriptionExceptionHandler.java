package com.tm.spring.questionnaire.subscription.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class SubscriptionExceptionHandler {

	@ExceptionHandler(ClientSideException.class)
	public ResponseEntity<String> clientSideExceptionResponse(ClientSideException ex) {
		String response = ex.getMessage();
        return new ResponseEntity<String>(response, HttpStatus.BAD_REQUEST);
    }
}
