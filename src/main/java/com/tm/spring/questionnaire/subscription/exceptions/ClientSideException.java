package com.tm.spring.questionnaire.subscription.exceptions;

public class ClientSideException extends RuntimeException {

	public ClientSideException(String message) {
        super(message);
    }
}
