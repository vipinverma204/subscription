package com.tm.spring.questionnaire.subscription.utility;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.tm.spring.questionnaire.subscription.constants.AppConstant;
import com.tm.spring.questionnaire.subscription.exceptions.ClientSideException;

@Service
public class Utility {

	public String getOrderDetails(HttpServletRequest request) {
		
		String orderDetail = "";
		Map<String, String[]> requestData = request.getParameterMap();
		
		if(!requestData.keySet().contains("order")) {
			throw new ClientSideException("Please specify order.");
		}
		
		orderDetail = request.getParameter("order");
		
		return orderDetail;
	}

	public String truncate(String orderDetails, int i) {
		
		String start = "";
		String end = "";
		
		if(i >= (AppConstant.truncatePart.length() + 2)) {
			if(orderDetails.length() >= i) {
				if(i%2 == 0) {
					start = orderDetails.substring(0, (i-AppConstant.truncatePart.length())/2);
					end = orderDetails.substring(orderDetails.length()-((i-AppConstant.truncatePart.length())/2 + 1));					
				}else {
					start = orderDetails.substring(0, (i-AppConstant.truncatePart.length())/2);
					end = orderDetails.substring(orderDetails.length()-((i-AppConstant.truncatePart.length())/2));
				}
				return start+AppConstant.truncatePart+end;
			}
		}
		return orderDetails;
	}
}
