package com.tm.spring.questionnaire.subscription.services.subscription;

import javax.servlet.http.HttpServletRequest;

public interface SubscriptionService {

	String subscribe(HttpServletRequest request);

}
