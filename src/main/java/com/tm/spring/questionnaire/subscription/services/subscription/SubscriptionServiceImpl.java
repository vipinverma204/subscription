package com.tm.spring.questionnaire.subscription.services.subscription;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tm.spring.questionnaire.subscription.constants.AppConstant;
import com.tm.spring.questionnaire.subscription.utility.Utility;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {

	private static final Logger logger = LoggerFactory.getLogger(SubscriptionServiceImpl.class);
	
	@Autowired
	Utility utility;
	
	@Override
	public String subscribe(HttpServletRequest request) {
		
		String response = "Subscription successful";
		String orderDetails = utility.getOrderDetails(request);
		
		String truncatedOrderDetails = utility.truncate(orderDetails, AppConstant.truncateLength);
		
		logger.debug(truncatedOrderDetails);
		
		response = truncatedOrderDetails;
		
		return response;
	}

}
