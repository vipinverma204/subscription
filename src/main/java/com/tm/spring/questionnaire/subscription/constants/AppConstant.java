package com.tm.spring.questionnaire.subscription.constants;

public class AppConstant {

	public static final String truncatePart = " ... (truncated) ... "; 
	
	public static final int truncateLength = 25;
}
