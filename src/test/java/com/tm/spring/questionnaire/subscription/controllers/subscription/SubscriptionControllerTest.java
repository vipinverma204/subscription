package com.tm.spring.questionnaire.subscription.controllers.subscription;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class SubscriptionControllerTest {
	
	@Autowired
    private MockMvc mockMvc;

	@Test
	@Category(com.tm.spring.questionnaire.subscription.controllers.subscription.SubscriptionControllerTest.class)
    public void shouldReturnTruncatedMessage() throws Exception {
		ResultMatcher ok = MockMvcResultMatchers.status().isOk();
		ResultMatcher content = MockMvcResultMatchers.content().string("12 ... (truncated) ... 90");
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/subscribe?order=123456789012345678901234567890");
		this.mockMvc.perform(requestBuilder).andDo(MockMvcResultHandlers.print()).andExpect(ok).andExpect(content);
    }
	
	@Test
    public void shouldReturnOriginalMessage() throws Exception {
		ResultMatcher ok = MockMvcResultMatchers.status().isOk();
		ResultMatcher content = MockMvcResultMatchers.content().string("1234567890");
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/subscribe?order=1234567890");
		this.mockMvc.perform(requestBuilder).andDo(MockMvcResultHandlers.print()).andExpect(ok).andExpect(content);
    }

}
